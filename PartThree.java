import java.util.Scanner;

public class PartThree
{
		public static void main(String[] args)
		{
			Scanner reader = new Scanner(System.in);
			AreaComputations ac = new AreaComputations();
			
			System.out.println("Please Input the Side of the Square");
			int side = reader.nextInt();
			
			System.out.println("Please Input the Length of the Rectangle");
			int length = reader.nextInt();
			
			System.out.println("Please Input the Width of the Rectangle");
			int width = reader.nextInt();
			
			System.out.println("The Area of the Square is: " + AreaComputations.areaSquare(side));
			System.out.println("The Area of the Rectangle is: " + ac.areaRectangle(length, width));
		}
}