public class MethodsTest
{
	public static void main (String[] args)
	{
		SecondClass sc = new SecondClass();
		int x = 6;
		int y = 3;
		double z = sumSquareRoot(x,y);
		System.out.println(SecondClass.addOne(50));
		System.out.println(sc.addTwo(50));
	}
	
	public static void methodNoInputNoReturn()
	{
		System.out.println("I’m in a method that takes no input and returns nothing");
		int x = 50;
		System.out.println(x);
	}
	
	public static void methodOneInputNoReturn(int youCanCallThisWhateverYouLike)
	{
		System.out.println("Inside the method one input no return");
		System.out.println(youCanCallThisWhateverYouLike);
	}
	
	public static void methodTwoInputNoReturn(int x, double y)
	{
		
	}
	
	public static int methodNoInputReturnInt()
	{
		return (6);
	}
	
	public static double sumSquareRoot(int x, int y)
	{
		double z = x + y;
		z = Math.sqrt(z);
		return (z);
	}
}